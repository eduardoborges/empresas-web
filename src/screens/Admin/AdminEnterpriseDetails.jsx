import React, { Component } from "react";

import AdminContext from "../../contexts/AdminContext";

const { Consumer } = AdminContext;

class WrapperComponent extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getEnterpriseItem(id);
  }

  render() {
    const item = this.props.state.searchItem;
    return (
      <div>
        {item && (
          <div>
            <h1>{item.enterprise_name}</h1>
            <b>{item.city}</b>
          </div>
        )}
      </div>
    );
  }
}

const AdminEnterpriseDetails = props => (
  <Consumer>{context => <WrapperComponent {...props} {...context} />}</Consumer>
);

export default AdminEnterpriseDetails;
