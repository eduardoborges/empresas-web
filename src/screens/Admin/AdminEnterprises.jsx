import React from "react";

import SearchBar from "../../components/SarchBar";
import AdminContext from "../../contexts/AdminContext";
import Link from "react-router-dom/Link";

const { Consumer } = AdminContext;

const AdminEnterprises = () => {
  return (
    <Consumer>
      {({ state }) => {
        return (
          <div>
            <h1>Lista de empresas</h1>
            <hr />
            <ul>
              {state.searchResults.length > 0 &&
                state.searchResults.map(item => (
                  <Link to={`/admin/empresas/${item.id}`}>
                    <li key={item.id}>{item.enterprise_name}</li>
                  </Link>
                ))}
            </ul>
          </div>
        );
      }}
    </Consumer>
  );
};

export default AdminEnterprises;
