import React, { Component } from "react";
import Auth from "../services/Auth";

class Login extends Component {
  state = {
    email: "testeapple@ioasys.com.br",
    password: "12341234",
    isLoading: false,
    error: false
  };

  componentDidMount = () => {
    if (Auth.isLoggedIn()) {
      console.log("O usuário já está autenticado, redirecionando...");
      this.props.history.push("/admin");
    } else {
      console.log(Auth.isLoggedIn());
    }
  };

  /**
   * Handle the login form
   * @param {Event} event The input event
   */
  doLogin = event => {
    event.preventDefault();
    const { email, password } = this.state;
    this.setState({ isLoading: true });
    Auth.login(email, password)
      .then(() => {
        this.props.history.push("/admin");
      })
      .catch(() => {
        this.setState({ error: true });
      });
  };

  /**
   * Handle change the input login form
   * @param {Event} event The input event
   */
  onChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <form onSubmit={this.doLogin}>
        <input
          type="email"
          name="email"
          onChange={this.onChangeInput}
          defaultValue={this.state.email}
        />
        <br />
        <input
          type="password"
          name="password"
          onChange={this.onChangeInput}
          defaultValue={this.state.password}
        />
        <br />
        {this.state.error && <b>Credênciais inválidas</b>}
        <br />
        <input
          type="submit"
          value={this.state.isLoading ? "Carregando..." : "Login"}
        />
      </form>
    );
  }
}

export default Login;
