import API from "./index";

class Enterprise {
  /**
   * Returns a list of enterprises
   */
  static index() {
    return API.get("/enterprises");
  }

  /**
   * Get a enterprise information
   * @param {String} enterpriseId An valid id
   */
  static show(enterpriseId = "") {
    return API.get(`/enterprises/${enterpriseId}`);
  }

  /**
   * Search a enterprise with some parameters
   * @param {Object} params A query string with parameters
   */
  static search(params = {}) {
    return API.get("/enterprises", {
      params: params
    });
  }
}

export default Enterprise;
