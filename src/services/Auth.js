import axios from "axios";

export default class Auth {
  /**
   * Check if current user is authenticaded
   * @returns {Boolean} User is authenticated?
   */
  static isLoggedIn() {
    const session = JSON.parse(localStorage.getItem("session"));
    let sessionSuccess = session.user.success ? true : false;
    let isLoggedIn = sessionSuccess ? true : false;
    return isLoggedIn;
  }

  /**
   * Execute an user login on system and save token
   *
   * @param {String} email The user username
   * @param {String} password The user password
   */
  static login(email = "", password = "") {
    const credentials = { email: email, password: password };

    return new Promise((resolve, reject) => {
      axios
        .post(
          `${process.env.REACT_APP_API_URI}/users/auth/sign_in`,
          credentials
        )
        .then(resp => {
          const tokens = {
            accessToken: resp.headers["access-token"],
            client: resp.headers["client"],
            uid: resp.headers["uid"],
            expiry: resp.headers["expiry"]
          };
          const sessionData = JSON.stringify({
            user: resp.data,
            tokens: tokens
          });
          localStorage.setItem("session", sessionData);
          resolve(true);
        })
        .catch(() => {
          reject(false);
        });
    });
  }

  /**
   * Do a refresh token of current user
   * @returns {Promise} Result
   */
  static refresh() {
    return false;
  }

  /**
   * Return an object with tokens: accessToken, client, uid
   * @returns { uid }
   */
  static getTokens() {
    const session = JSON.parse(localStorage.getItem("session"));
    return {
      "access-token": session.tokens.accessToken,
      client: session.tokens.client,
      uid: session.tokens.uid
    };
  }
}
