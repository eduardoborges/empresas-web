// Its the base of services
import axios from "axios";
import Auth from "./Auth";

const API = axios.create({
  baseURL: process.env.REACT_APP_API_URI,
  headers: Auth.getTokens()
});

export default API;
