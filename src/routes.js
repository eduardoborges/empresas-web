import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

// Services
import Auth from "./services/Auth";

// Containers
import MainContainer from "./containers/MainContainer";
import AdminContainer from "./containers/AdminContainer";

// Simple Screens
import Login from "./screens/Login";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={MainContainer} />
      <Route path="/login" exact component={Login} />
      <PrivateRoute path="/admin" component={AdminContainer} />
      <Route path="*" render={() => <h1>Not Found</h1>} />
    </Switch>
  );
};

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      Auth.isLoggedIn() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export default Routes;
