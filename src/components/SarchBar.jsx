import React from "react";

import AdminContext from "../contexts/AdminContext";

const { Consumer } = AdminContext;

const SarchBar = () => (
  <Consumer>
    {({ onSarchInputChange, doSearch }) => {
      return (
        <form onSubmit={doSearch}>
          <input type="text" onChange={onSarchInputChange} />
          <button>Search</button>
        </form>
      );
    }}
  </Consumer>
);

export default SarchBar;
