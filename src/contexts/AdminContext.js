import { createContext } from "react";

const context = {
  state: {
    searchInput: "",
    searchResults: []
  },
  getEnterpriseIndex: () => {},
  getEnterpriseItem: () => {},
  getEnterpriseSearch: () => {},
  onSarchInputChange: () => {},
  doSearch: () => {}
};

export default createContext(context);
