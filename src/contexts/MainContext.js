import { createContext } from "react";

const context = {
  state: () => {},
  doSomething: () => {}
};

export default createContext(context);
