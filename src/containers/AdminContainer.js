import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// Context
import AdminContext from "../contexts/AdminContext";

// Services
import Enterprise from "../services/Enterprise";

// screens
import AdminEnterprises from "../screens/Admin/AdminEnterprises";
import AdminEnterpriseDetails from "../screens/Admin/AdminEnterpriseDetails";

// components
import SearchBar from "../components/SarchBar";

// context
const { Provider } = AdminContext;

class AdminContainer extends Component {
  state = {
    searchInput: "",
    searchResults: [],
    isLoading: false
  };

  componentWillMount = () => {
    // this.getEnterpriseIndex();
    // this.getEnterpriseItem(1);
    // this.getEnterpriseSearch({ enterprise_types: "1", name: "aQm" });
  };

  getEnterpriseIndex = () => {
    Enterprise.index().then(resp => {
      this.setState({ isLoading: true });
      console.log(resp.data);
      this.setState({
        searchResults: resp.data.enterprises,
        isLoading: false
      });
    });
  };

  getEnterpriseItem = id => {
    this.setState({ isLoading: true });
    Enterprise.show(id).then(resp => {
      console.log(resp.data);
      this.setState({ searchItem: resp.data.enterprise, isLoading: false });
    });
  };

  getEnterpriseSearch = query => {
    this.setState({ isLoading: true });
    Enterprise.search(query).then(resp => {
      console.log(resp.data);
      this.setState({
        searchResults: resp.data.enterprises,
        isLoading: false
      });
    });
  };

  onSarchInputChange = event => {
    event.preventDefault();
    this.setState({ searchInput: event.target.value });
  };

  doSearch = event => {
    event.preventDefault();
    this.props.history.push("/admin/empresas");
    this.getEnterpriseSearch({ name: this.state.searchInput });
  };

  render = () => (
    <Provider value={{ ...this }}>
      <SearchBar />
      <hr />
      <Switch>
        <Redirect path="/admin" exact to="/admin/empresas" />
        <Route path="/admin/empresas" exact component={AdminEnterprises} />
        <Route path="/admin/empresas/:id" component={AdminEnterpriseDetails} />
      </Switch>
    </Provider>
  );
}

export default AdminContainer;
